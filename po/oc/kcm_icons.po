# translation of kcmicons.po to Occitan (lengadocian)
# Occitan translation of kcmicons.po
#
# Yannig MARCHEGAY (Kokoyaya) <yannig@marchegay.org> - 2006-2007
#
# Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kcmicons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-22 00:47+0000\n"
"PO-Revision-Date: 2008-08-05 22:26+0200\n"
"Last-Translator: Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>\n"
"Language-Team: Occitan (lengadocian) <ubuntu-l10n-oci@lists.ubuntu.com>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: KBabel 1.11.4\n"

#: iconsizecategorymodel.cpp:14
#, kde-format
msgid "Main Toolbar"
msgstr "Barra d'espleches principala"

#: iconsizecategorymodel.cpp:15
#, fuzzy, kde-format
#| msgid "Main Toolbar"
msgid "Secondary Toolbars"
msgstr "Barra d'espleches principala"

#: iconsizecategorymodel.cpp:16
#, kde-format
msgid "Small Icons"
msgstr ""

#: iconsizecategorymodel.cpp:17
#, kde-format
msgid "Dialogs"
msgstr ""

#. i18n: ectx: label, entry (Theme), group (Icons)
#: iconssettingsbase.kcfg:9
#, kde-format
msgid "Name of the current icon theme"
msgstr ""

#. i18n: ectx: label, entry (desktopSize), group (DesktopIcons)
#: iconssettingsbase.kcfg:15
#, kde-format
msgid "Desktop icons size"
msgstr ""

#. i18n: ectx: label, entry (toolbarSize), group (ToolbarIcons)
#: iconssettingsbase.kcfg:21
#, kde-format
msgid "Toolbar icons size"
msgstr ""

#. i18n: ectx: label, entry (mainToolbarSize), group (MainToolbarIcons)
#: iconssettingsbase.kcfg:27
#, fuzzy, kde-format
#| msgid "Main Toolbar"
msgid "Main toolbar icons size"
msgstr "Barra d'espleches principala"

#. i18n: ectx: label, entry (smallSize), group (SmallIcons)
#: iconssettingsbase.kcfg:33
#, kde-format
msgid "Small icons size"
msgstr ""

#. i18n: ectx: label, entry (panelSize), group (PanelIcons)
#: iconssettingsbase.kcfg:39
#, kde-format
msgid "Panel icons size"
msgstr ""

#. i18n: ectx: label, entry (dialogSize), group (DialogIcons)
#: iconssettingsbase.kcfg:45
#, kde-format
msgid "Dialog icons size"
msgstr ""

#: main.cpp:182
#, kde-format
msgid "Unable to create a temporary file."
msgstr ""

#: main.cpp:193
#, kde-format
msgid "Unable to download the icon theme archive: %1"
msgstr ""

#: main.cpp:207
#, kde-format
msgid "The file is not a valid icon theme archive."
msgstr ""

#: main.cpp:212
#, kde-format
msgid ""
"A problem occurred during the installation process; however, most of the "
"themes in the archive have been installed"
msgstr ""

#: main.cpp:216
#, kde-format
msgid "Theme installed successfully."
msgstr ""

#: main.cpp:260
#, fuzzy, kde-format
#| msgid "Install New Theme..."
msgid "Installing icon themes…"
msgstr "Installar un tèma novèl..."

#: main.cpp:270
#, fuzzy, kde-format
#| msgid "Install New Theme..."
msgid "Installing %1 theme…"
msgstr "Installar un tèma novèl..."

#: package/contents/ui/IconSizePopup.qml:34
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Here you can configure the default sizes of various icon types at a system-"
"wide level. Note that not all apps will respect these settings.<nl/><nl/>If "
"you find that objects on screen are generally too small or too large, "
"consider adjusting the global scale instead."
msgstr ""

#: package/contents/ui/IconSizePopup.qml:41
#, kde-format
msgid "Adjust Global Scale…"
msgstr ""

#: package/contents/ui/IconSizePopup.qml:117
#, fuzzy, kde-format
#| msgid "Size:"
msgctxt "@label:slider"
msgid "Size:"
msgstr "Talha :"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "This module allows you to choose the icons for your desktop."
msgstr ""

#: package/contents/ui/main.qml:148
#, kde-format
msgid "Remove Icon Theme"
msgstr ""

#: package/contents/ui/main.qml:155
#, kde-format
msgid "Restore Icon Theme"
msgstr ""

#: package/contents/ui/main.qml:229
#, kde-format
msgid "Configure Icon Sizes"
msgstr ""

#: package/contents/ui/main.qml:246
#, fuzzy, kde-format
#| msgid "Install New Theme..."
msgid "Install from File…"
msgstr "Installar un tèma novèl..."

#: package/contents/ui/main.qml:251
#, fuzzy, kde-format
#| msgid "Install New Theme..."
msgid "Get New Icons…"
msgstr "Installar un tèma novèl..."

#: package/contents/ui/main.qml:273
#, kde-format
msgctxt "@title:window"
msgid "Configure Icon Sizes"
msgstr ""

#: package/contents/ui/main.qml:285
#, kde-format
msgid "Open Theme"
msgstr ""

#: package/contents/ui/main.qml:287
#, kde-format
msgid "Theme Files (*.tar.gz *.tar.bz2)"
msgstr ""

#~ msgid "Toolbar"
#~ msgstr "Barra d'espleches"

#~ msgid "Panel"
#~ msgstr "Panèl"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Yannig Marchegay (Kokoyaya)"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "yannig@marchegay.org"

#~ msgid "Icons"
#~ msgstr "Icònas"

#~ msgid "Geert Jansen"
#~ msgstr "Geert Jansen"

#~ msgid "Torsten Rahn"
#~ msgstr "Torsten Rahn"

#~ msgid "Use of Icon"
#~ msgstr "Type d'icône"

#, fuzzy
#~| msgid "Default"
#~ msgctxt "@label The icon rendered by default"
#~ msgid "Default"
#~ msgstr "Defaut"

#, fuzzy
#~| msgid "Active"
#~ msgctxt "@label The icon rendered as active"
#~ msgid "Active"
#~ msgstr "Actiu"

#, fuzzy
#~| msgid "Disabled"
#~ msgctxt "@label The icon rendered as disabled"
#~ msgid "Disabled"
#~ msgstr "Desactivat"

#~ msgid "No Effect"
#~ msgstr "Pas d'efièch"

#~ msgid "Gamma"
#~ msgstr "Gammà"

#~ msgid "Desaturate"
#~ msgstr "Desaturar"

#~ msgid "Preview"
#~ msgstr "Ulhada"

#~ msgid "Name"
#~ msgstr "Nom"

#~ msgid "Description"
#~ msgstr "Descripcion"

#~ msgid "Confirmation"
#~ msgstr "Confirmacion"

#~ msgid "Ad&vanced"
#~ msgstr "A&vançat"
